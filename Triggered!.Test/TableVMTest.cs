﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using Triggered_;

namespace Triggered_.Test
{
    [TestClass]
    public class TableVMTest
    {
        const string TestDbName = "TrigTest";
        const string TestTableName = "TestTable";
        public Database ArrangeLocalDb()
        {
            var server = new Server("(LocalDB)\\MSSQLLocalDB");
            if (server.Databases.Contains(TestDbName))
            {
                server.Databases[TestDbName].Drop();
            }

            var database = new Database(server, TestDbName);
            database.Create();

            var table = new Table();
            table.Name = TestTableName;
            table.Parent = database;

            table.Columns.Add(new Column(table, "id_test", DataType.Int)
            {
                Identity = true,
                IdentityIncrement = 1,
                IdentitySeed = 1,
                Nullable = false
            });
            Index pk = new Index(table, string.Format("PK_{0}", table.Name))
            {
                IndexKeyType = IndexKeyType.DriPrimaryKey,
                IsClustered = true,
                IsUnique = true
            };
            pk.IndexedColumns.Add(new IndexedColumn(pk, "id_test"));
            table.Indexes.Add(pk);

            table.Columns.Add(new Column(table, "test_string", DataType.VarCharMax) { Nullable = false });
            table.Columns.Add(new Column(table, "test_int", DataType.Int) { Nullable = false });

            table.Create();

            return database;
        }
        [TestMethod]
        public void TestLogTableCreation()
        {
            // arrange
            var database = ArrangeLocalDb();

            var vm = new TableViewModel(database.Tables[TestTableName]);

            //act
            vm.HasLogsTable = true;

            // assert
            Assert.IsTrue(database.Tables.Contains($"Log_{TestTableName}"));
            var table = database.Tables[$"Log_{TestTableName}"];
            Assert.IsNotNull(table.Columns["id_log"]);
            Assert.IsNotNull(table.Columns["log_time"]);
            Assert.IsNotNull(table.Columns["log_user"]);
            Assert.IsNotNull(table.Columns["log_operation"]);
            Assert.IsNotNull(table.Columns["test_string"]);
            Assert.IsNotNull(table.Columns["test_int"]);
        }
        [TestMethod]
        public void TestTrigCreation()
        {
            // arrange
            var database = ArrangeLocalDb();

            var vm = new TableViewModel(database.Tables["TestTable"]);

            //act
            vm.HasLogsTable = true;
            vm.HasInsertTrigger = true;
            vm.HasDeleteTrigger = true;
            vm.HasUpdateTrigger = true;

            // assert
            Assert.IsTrue(vm.HasInsertTrigger);
            Assert.IsTrue(vm.HasDeleteTrigger);
            Assert.IsTrue(vm.HasUpdateTrigger);
        }
    }
}
