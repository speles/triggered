﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Triggered_
{
    /// <summary>
    /// Interaction logic for ColumnSelectionWindow.xaml
    /// </summary>
    public partial class ColumnSelectionWindow : Window
    {
        TableViewModel SelectedTableVM;
        public bool[] ColumnSelection;

        public ColumnSelectionWindow(TableViewModel selectedTableVM)
        {
            InitializeComponent();
            SelectedTableVM = selectedTableVM;
            ColumnSelection = new bool[selectedTableVM.TableModel.Columns.Count];
            for (int  i = 0; i < ColumnSelection.Count(); i++)
            {
                ColumnSelection[i] = true;
            }
        }
        public bool IsColumnSelected
        {
            get { return (bool)GetValue(IsCheckBoxCheckedProperty); }
            set { SetValue(IsCheckBoxCheckedProperty, value); }
        }

        public static readonly DependencyProperty IsCheckBoxCheckedProperty =
DependencyProperty.Register("IsCheckBoxChecked", typeof(bool),
typeof(ColumnSelectionWindow), new UIPropertyMetadata(false));

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
