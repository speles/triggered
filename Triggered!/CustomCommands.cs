﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Triggered_
{
    public static class CustomCommands
    {
        public static readonly RoutedUICommand ClearLog = new RoutedUICommand
            (
            "Очистити",
            "Очистити",
            typeof(CustomCommands)
            );

        public static readonly RoutedUICommand ExportLog = new RoutedUICommand
            (
            "Експорт",
            "Експорт",
            typeof(CustomCommands)
            );

        public static readonly RoutedUICommand CreateTriggers = new RoutedUICommand
            (
            "Створити",
            "Створити",
            typeof(CustomCommands)
            );

        public static readonly RoutedUICommand DeleteTriggers = new RoutedUICommand
            (
            "Видалити",
            "Видалити",
            typeof(CustomCommands)
            );

        public static readonly RoutedUICommand NewConnection = new RoutedUICommand
            (
            "Нове підключення",
            "Нове підключення",
            typeof(CustomCommands)
            );

        public static readonly RoutedUICommand Help = new RoutedUICommand
            (
            "Допомога",
            "Допомога",
            typeof(CustomCommands),
            new InputGestureCollection()
                {
                    new KeyGesture(Key.F1)
                }
            );

        public static readonly RoutedUICommand Info = new RoutedUICommand
            (
            "Про програму",
            "Про програму",
            typeof(CustomCommands)
            );

        public static readonly RoutedUICommand Exit = new RoutedUICommand
            (
            "Вихід",
            "Вихід",
            typeof(CustomCommands),
            new InputGestureCollection()
                {
                    new KeyGesture(Key.F4, ModifierKeys.Alt)
                }
            );
    }
}
