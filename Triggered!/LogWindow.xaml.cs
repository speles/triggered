﻿using Microsoft.SqlServer.Management.Smo;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Triggered_
{
    /// <summary>
    /// Interaction logic for LogWindow.xaml
    /// </summary>
    public partial class LogWindow : Window
    {
        static private Brush AddedBrush = new SolidColorBrush(Color.FromRgb(130, 255, 146));
        static private Brush DeletedBrush = new SolidColorBrush(Color.FromRgb(255, 130, 130));
        static private Brush UpdatedBrush = new SolidColorBrush(Color.FromRgb(255, 255, 130));

        public TableViewModel LogTable;
        public DataSet LogData;
        public Command FilterByIdCommand { get; set; }
        public Command FilterByUserCommand { get; set; }
        public LogWindow()
        {
            FilterByIdCommand = new Command
            {
                CanExecuteDelegate = (o) =>
                {
                    return dgLog.SelectedIndex >= 0;
                },
                ExecuteDelegate = (o) =>
                {
                    LogWindow logWindow = new LogWindow();
                    int id = -1;
                    foreach (System.Data.DataRowView item in dgLog.ItemsSource)
                    {
                        var row = dgLog.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                        if (row.IsSelected)
                        {
                            id = Convert.ToInt32(item[LogTable.GetPKName()]);
                        }
                    }
                    logWindow.Title = $"Журнал змін для запису \"{LogTable.GetPKName()}={id}\"";
                    logWindow.LogTable = LogTable;
                    logWindow.LogData = LogTable.RunSqlRequest("SELECT * FROM " + LogTable.GetLogTableName() + $" WHERE {LogTable.GetPKName()}={id}");
                    if (logWindow.LogData != null)
                    {
                        logWindow.Show();
                    }
                }
            };
            DataContext = this;

            FilterByUserCommand = new Command
            {
                CanExecuteDelegate = (o) =>
                {
                    return dgLog.SelectedIndex >= 0;
                },
                ExecuteDelegate = (o) =>
                {
                    LogWindow logWindow = new LogWindow();
                    string us = "";

                    foreach (System.Data.DataRowView item in dgLog.ItemsSource)
                    {
                        var row = dgLog.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                        if (row.IsSelected)
                        {
                            us = item["log_user"].ToString();
                        }
                    }
                    logWindow.Title = $"Журнал змін користувача" + us;
                    logWindow.LogTable = LogTable;
                    logWindow.LogData = LogTable.RunSqlRequest("SELECT * FROM " + LogTable.GetLogTableName() + $" WHERE log_user='{us}'");
                    if (logWindow.LogData != null)
                    {
                        logWindow.Show();
                    }
                }
            };
            DataContext = this;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataTable table = LogData.Tables[0];
            DataView dv = table.AsDataView();
            dgLog.ItemsSource = dv;
            dgLog.UpdateLayout();

            int insertsCnt = 0, updatesCnt = 0, deletesCnt = 0;

            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["log_operation"].Equals("INSERT"))
                {
                    insertsCnt++;
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        dgLog.GetCell(i, j).Background = AddedBrush;
                    }
                } else if (table.Rows[i]["log_operation"].Equals("DELETE"))
                {
                    deletesCnt++;
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        dgLog.GetCell(i, j).Background = DeletedBrush;
                    }
                }
                else if (table.Rows[i]["log_operation"].ToString().StartsWith("UPDATE"))
                {
                    if (i % 2 == 0) updatesCnt++;
                    int other = table.Rows[i]["log_operation"].ToString().EndsWith("OLD") ? i + 1 : i - 1;
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (!table.Rows[i][j].Equals(table.Rows[other][j]))
                            dgLog.GetCell(i, j).Background = UpdatedBrush;
                    }
                }
            }

            tbStats.Text = "";

            int total = insertsCnt + updatesCnt + deletesCnt;

            tiStats.IsEnabled = total > 0;
            
            if (total > 0)
            {
                tbStats.Text += $"Кількість INSERT: {insertsCnt} ({insertsCnt * 100 / total}%)\r\n";
                tbStats.Text += $"Кількість UPDATE: {updatesCnt} ({updatesCnt * 100 / total}%)\r\n";
                tbStats.Text += $"Кількість DELETE: {deletesCnt} ({deletesCnt * 100 / total}%)\r\n";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog logSaveDialog = new SaveFileDialog();
            logSaveDialog.Filter = "Coma-separated files | *.csv";
            if (logSaveDialog.ShowDialog() == true)
            {
                var sb = new StringBuilder();

                sb.AppendLine(string.Join(",", dgLog.Columns.Select(column => $"\"{column.Header}\"").ToArray()));

                foreach (System.Data.DataRowView row in dgLog.ItemsSource)
                {
                    List<string> tmp = new List<string>();
                    for (int i = 0; i < dgLog.Columns.Count; i++)
                        tmp.Add($"\"{row[i]}\"");
                    sb.AppendLine(string.Join(",", tmp.ToArray()));
                }
                File.WriteAllText(logSaveDialog.FileName, sb.ToString());
            }
        }
    }
}
