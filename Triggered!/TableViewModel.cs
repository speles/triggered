﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Triggered_
{
    public class TableViewModel: ViewModelBase
    {
        public Table TableModel;
        public string Name { get { return TableModel.Name; } }
        bool[] ColumnSelection;

        public TableViewModel(Table _model)
        {
            TableModel = _model;
            ColumnSelection = new bool[TableModel.Columns.Count];
            for (int i = 0; i < ColumnSelection.Count(); i++)
            {
                ColumnSelection[i] = true;
            }

            ShowLogCommand = new Command()
            {
                CanExecuteDelegate = (args) => HasLogsTable,
                ExecuteDelegate = (args) =>
                {
                    LogWindow logWindow = new LogWindow();
                    logWindow.Title = $"Журнал змін таблиці \"{Name }\"";
                    logWindow.LogTable = this;
                    logWindow.LogData = RunSqlRequest("SELECT * FROM " + GetLogTableName());
                    if (logWindow.LogData != null)
                    {
                        logWindow.Show();
                    }
                }
            };

            ColumnSelectionCommand = new Command()
            {
                ExecuteDelegate = (args) =>
                {
                    ColumnSelectionWindow csWindow = new ColumnSelectionWindow(this);
                    csWindow.Title = $"Вибір стовпців таблиці \"{Name }\"";
                    if (csWindow.ShowDialog() == true)
                    {
                        ColumnSelection = csWindow.ColumnSelection;
                    }
                }
            };
        }
        
        public bool HasLogsTable {
            get {
                return TableModel.Parent.Tables.Cast<Table>().Any(t => t.Name == GetLogTableName());
            }
            set
            {
                if (value == HasLogsTable) return;
                if (value)
                    CreateLogTable();
                else
                {
                    DeleteLogTable();
                    HasInsertTrigger = false;
                    HasUpdateTrigger = false;
                    HasDeleteTrigger = false;
                }
                CommandManager.InvalidateRequerySuggested();
                TableModel.Parent.Tables.Refresh();
                OnPropertyChanged("HasLogsTable");
            }
        }
        public bool HasInsertTrigger {
            get {
                return TableModel.Triggers.Cast<Microsoft.SqlServer.Management.Smo.Trigger>().Any(t => t.Name == $"{TableModel.Name}_Insert_Log_Trigger");
            }
            set
            {
                if (value == HasInsertTrigger) return;
                if (value)
                    CreateInsertTrigger();
                else
                    DeleteInsertTrigger();
                TableModel.Triggers.Refresh();
                OnPropertyChanged("HasInsertTrigger");
            }
        }
        public bool HasUpdateTrigger {
            get {
                return TableModel.Triggers.Cast<Microsoft.SqlServer.Management.Smo.Trigger>().Any(t => t.Name == $"{TableModel.Name}_Update_Log_Trigger");
            }
            set
            {
                if (value == HasUpdateTrigger) return;
                if (value)
                    CreateUpdateTrigger();
                else
                    DeleteUpdateTrigger();
                TableModel.Triggers.Refresh();
                OnPropertyChanged("HasUpdateTrigger");
            }
        }
        public bool HasDeleteTrigger {
            get {
                return TableModel.Triggers.Cast<Microsoft.SqlServer.Management.Smo.Trigger>().Any(t => t.Name == $"{TableModel.Name}_Delete_Log_Trigger");
            }
            set
            {
                if (value == HasDeleteTrigger) return;
                if (value)
                    CreateDeleteTrigger();
                else
                    DeleteDeleteTrigger();
                TableModel.Triggers.Refresh();
                OnPropertyChanged("HasDeleteTrigger");
            }
        }
        public string GetLogTableName()
        {
            return "Log_" + Name;
        }

        public string GetPKName()
        {
            Column primary = (from c in TableModel.Columns.Cast<Column>() where c.InPrimaryKey == true select c).FirstOrDefault();
            return primary.Name;
        }

        public ICommand ShowLogCommand { get; set; }
        public ICommand ColumnSelectionCommand { get; set; }

        private void CreateLogTable() {
            Table log = new Table();
            log.Name = GetLogTableName();
            log.Parent = TableModel.Parent;

            log.Columns.Add(new Column(log, "id_log", DataType.Int)
            {
                Identity = true,
                IdentityIncrement = 1,
                IdentitySeed = 1,
                Nullable = false,
            });

            Index pk = new Index(log, string.Format("PK_{0}", log.Name))
            {
                IndexKeyType = IndexKeyType.DriPrimaryKey,
                IsClustered = true,
                IsUnique = true
            };

            pk.IndexedColumns.Add(new IndexedColumn(pk, "id_log"));
            log.Indexes.Add(pk);

            Column primary = (from c in TableModel.Columns.Cast<Column>() where c.InPrimaryKey == true select c).FirstOrDefault();
            if (primary != null)
                log.Columns.Add(new Column(log, primary.Name, primary.DataType) { Nullable = false });

            IEnumerable<Column> col = from co in TableModel.Columns.Cast<Column>() where co.InPrimaryKey != true select co;
            if (col != null)
                foreach (Column cols in col)
                {
                    log.Columns.Add(new Column(log, cols.Name, cols.DataType) { Nullable = true });
                }

            log.Columns.Add(new Column(log, "log_time", DataType.DateTime) { Nullable = false });
            log.Columns.Add(new Column(log, "log_user", DataType.VarChar(30)) { Nullable = false });
            log.Columns.Add(new Column(log, "log_operation", DataType.VarChar(30)) { Nullable = false });

            log.Create();
        }
        private void DeleteLogTable()
        {
            RunSqlRequest("DROP TABLE " + GetLogTableName());
        }
        private void CreateInsertTrigger()
        {
            StringBuilder logtrigger = new StringBuilder();
            logtrigger.Append(string.Format("CREATE TRIGGER [{0}].[{1}_Insert_Log_Trigger] \n", TableModel.Schema, TableModel.Name));
            logtrigger.Append(string.Format("ON [{0}].[{1}] AFTER INSERT \n", TableModel.Schema, TableModel.Name));
            logtrigger.Append("AS \n");
            logtrigger.Append("Begin \n");
            logtrigger.Append(string.Format("INSERT INTO {0} \n", GetLogTableName()));
            logtrigger.Append("SELECT *, GETDATE(), USER_NAME(), 'INSERT' FROM inserted as i \n");
            logtrigger.Append("END");
            RunSqlRequest(logtrigger.ToString());
        }
        private void DeleteInsertTrigger()
        {
            RunSqlRequest(string.Format("DROP TRIGGER [{0}].[{1}_Insert_Log_Trigger] \n", TableModel.Schema, TableModel.Name));
        }
        private void CreateUpdateTrigger()
        {
            StringBuilder logtrigger = new StringBuilder();
            logtrigger.Append(string.Format("CREATE TRIGGER [{0}].[{1}_Update_Log_Trigger] \n", TableModel.Schema, TableModel.Name));
            logtrigger.Append(string.Format("ON [{0}].[{1}] AFTER UPDATE \n", TableModel.Schema, TableModel.Name));
            logtrigger.Append("AS \n");
            logtrigger.Append("Begin \n");
            logtrigger.Append(string.Format("INSERT INTO {0} \n", GetLogTableName()));
            logtrigger.Append("SELECT *, GETDATE(), USER_NAME(), 'UPDATE OLD' FROM deleted as d \n");
            logtrigger.Append(string.Format("INSERT INTO {0} \n", GetLogTableName()));
            logtrigger.Append("SELECT *, GETDATE(), USER_NAME(), 'UPDATE NEW' FROM inserted as i \n");
            logtrigger.Append("END");
            RunSqlRequest(logtrigger.ToString());
        }
        private void DeleteUpdateTrigger()
        {
            RunSqlRequest(string.Format("DROP TRIGGER [{0}].[{1}_Update_Log_Trigger] \n", TableModel.Schema, TableModel.Name));
        }
        private void CreateDeleteTrigger()
        {
            StringBuilder logtrigger = new StringBuilder();
            logtrigger.Append(string.Format("CREATE TRIGGER [{0}].[{1}_Delete_Log_Trigger] \n", TableModel.Schema, TableModel.Name));
            logtrigger.Append(string.Format("ON [{0}].[{1}] AFTER DELETE \n", TableModel.Schema, TableModel.Name));
            logtrigger.Append("AS \n");
            logtrigger.Append("Begin \n");
            logtrigger.Append(string.Format("INSERT INTO {0} \n", GetLogTableName()));
            logtrigger.Append("SELECT *, GETDATE(), USER_NAME(), 'DELETE' FROM deleted as d \n");
            logtrigger.Append("END");
            RunSqlRequest(logtrigger.ToString());
        }
        private void DeleteDeleteTrigger()
        {
            RunSqlRequest(string.Format("DROP TRIGGER [{0}].[{1}_Delete_Log_Trigger] \n", TableModel.Schema, TableModel.Name));
        }
        public DataSet RunSqlRequest(string request)
        {
            try
            {
                return TableModel.Parent.ExecuteWithResults(request);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.InnerException.Message, "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
    }
}
