﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace Triggered_
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    public partial class ConnectionWindow : Window
    {
        public ConnectionWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string connectionString = $"Data Source={tbServer.Text};Initial Catalog={tbDatabase.Text};Persist Security Info=False;User ID={tbLogin.Text};Password={tbPassword.Password}";
            SqlConnection connection = new SqlConnection(connectionString);
            ServerConnection _serverconnection = new ServerConnection(connection);
            try
            {
                Server server = new Server(_serverconnection);
                Database database = server.Databases[tbDatabase.Text];
                MainWindow mainWindow = new MainWindow();
                mainWindow.Db = new Server(_serverconnection).Databases[tbDatabase.Text];
                mainWindow.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка з'єднання", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void tbDatabase_DropDownOpened(object sender, EventArgs e)
        {
            string connectionString = $"Data Source={tbServer.Text};Persist Security Info=False;User ID={tbLogin.Text};Password={tbPassword.Password};Connect timeout=2";
            SqlConnection connection = new SqlConnection(connectionString);

            ServerConnection _serverconnection = new ServerConnection(connection);
            tbDatabase.Items.Clear();
            try
            {
                Server server = new Server(_serverconnection);
                foreach (var db in server.Databases.Cast<Database>())
                {
                    tbDatabase.Items.Add(db.Name);
                }
            }
            catch (Exception ex)
            {
                tbDatabase.Items.Add("*не вдалося під'єднатися до сервера*");
            }
        }
    }
}
