﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Triggered_
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ServerConnection ServerConnection;
        public Database Db;

        public TableViewModel SelectedTable { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void miNewConnection_Click(object sender, RoutedEventArgs e)
        {
                   
        }

        public bool? IsUpdateChecked
        {
            get { return (bool?)GetValue(IsCheckBoxCheckedProperty); }
            set { SetValue(IsCheckBoxCheckedProperty, value); }
        }

        public bool? IsInsertChecked
        {
            get { return (bool?)GetValue(IsCheckBoxCheckedProperty); }
            set { SetValue(IsCheckBoxCheckedProperty, value); }
        }

        public bool? IsDeleteChecked
        {
            get { return (bool?)GetValue(IsCheckBoxCheckedProperty); }
            set { SetValue(IsCheckBoxCheckedProperty, value); }
        }

        public static readonly DependencyProperty IsCheckBoxCheckedProperty = DependencyProperty.Register("IsCheckBoxChecked", typeof(bool?), typeof(MainWindow), new UIPropertyMetadata(false));

        private void cbExit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void cbExit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void cbClearLog_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void cbExportLog_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void cbCreateTriggers_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void cbDeleteTriggers_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void cbNewConnection_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void cbinfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void cbHelp_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IEnumerable<Table> tablesList = Db.Tables.Cast<Table>().Where(t => !t.Name.StartsWith("Log_"));
            ObservableCollection<TableViewModel> tablesVM = new ObservableCollection<TableViewModel>(tablesList.Select(t => new TableViewModel(t)));
            dgTables.ItemsSource = tablesVM;
        }

        private void cbInsert_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void cbUpdate_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void cbDelete_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void miEN_Click(object sender, RoutedEventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-GB");
            foreach (Window win in Application.Current.Windows)
            {
                win.InvalidateVisual();
            }
        }

        private void miUA_Click(object sender, RoutedEventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("uk-UA");
            foreach (Window win in Application.Current.Windows)
            {
                win.InvalidateVisual();
            }
        }
    }
}
